<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
</head>
<body>
<div class="jumbotron">
    <div class="d-flex justify-content-center">
        <h1 style="font-family: Nunito; color:#636b6f">Api</h1>
    </div>
    <div class="container">
        <table class="table table-bordered table-sm">
                <thead class="table-dark" style="text-align: center; color:#636b6f">
                  <tr>
                    <th>Id</th  >
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Edad</th>
                    <th>Celular</th>
                    </th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($posts->listaUsuarios as $post)
                    <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$post->nombre}}</td>
                    <td>{{$post->apellido}}</td>
                    <td>{{$post->edad}}</td>
                    <td>{{$post->celular}}</td>
                    </tr>
                    @endforeach

                </tbody>
              </table>
    </div>
</div>
</body>
</html>
